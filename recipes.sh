#! /bin/bash

post_install () {
	systemctl --user --global --quiet enable "skull.service"
}

post_upgrade () {
	post_install
}

pre_remove () {
	systemctl --user --global --quiet disable "skull.service"
}
